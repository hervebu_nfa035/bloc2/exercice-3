package fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media;

import java.io.RandomAccessFile;

/**
 *
 */
public interface WalletFrameMedia extends ResumableImageFrameMedia<RandomAccessFile> {
    /**
     *
     * @return : nombre de lignes du wallet
     */
    long getNumberOfLines();

    /**
     * incremente le nombre de lignes.
     */
    void incrementLines();

}
