package fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.impl.db;

import fr.cnam.foad.nfa035.badges.wallet.dao.impl.DirectAccessBadgeWalletDAOImpl;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.DirectAccessDatabaseDeserializer;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadgeMetadata;
import org.apache.logging.log4j.Logger;

import java.io.OutputStream;
import java.util.List;

public class WalletDeserializerDirectAccessImpl implements DirectAccessDatabaseDeserializer {
    List<DigitalBadgeMetadata> metas;
    Logger Log;
    OutputStream sourceOutputStream;

    public WalletDeserializerDirectAccessImpl () {
        Log.info(" Wallet Deserializer");
    }

}
