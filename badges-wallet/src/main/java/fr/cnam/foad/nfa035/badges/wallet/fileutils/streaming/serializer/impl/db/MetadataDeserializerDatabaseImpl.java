package fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.impl.db;

import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.WalletFrameMedia;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.MetadataDeserializer;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadgeMetadata;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.List;

public class MetadataDeserializerDatabaseImpl implements MetadataDeserializer {
    @Override
    public List<DigitalBadgeMetadata> deserialize(WalletFrameMedia media) throws IOException {
        List<DigitalBadgeMetadata> metas;
        // A COMPLéTER
        List<DigitalBadgeMetadata> metas1 = metas;
        return metas1;
    }

    /**
     * A COMMENTER
     */
    public static String readLastLine(RandomAccessFile file) throws IOException {
        StringBuilder builder = new StringBuilder();
        long length = file.length();
        for (long seek = length; seek >= 0; --seek) {
            file.seek(seek);
            char c = (char) file.read();
            if (c != '\n' || seek == 1) {
                builder.append(c);
            } else {
                builder = builder.reverse();
                break;
            }
        }
        return builder.toString();
    }
}
