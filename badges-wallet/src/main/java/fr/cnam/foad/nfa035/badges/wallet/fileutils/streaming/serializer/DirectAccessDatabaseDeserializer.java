package fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer;

import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.WalletFrameMedia;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadgeMetadata;

import java.io.IOException;
import java.io.OutputStream;

/**
 * {@inheritDoc}
 */
public interface DirectAccessDatabaseDeserializer extends DatabaseDeserializer<WalletFrameMedia>{
    /**
     * {@inheritDoc}
     */
    void deserialize(WalletFrameMedia media, DigitalBadgeMetadata meta) throws IOException;
    /**
     * {@inheritDoc}
     */
    <T extends OutputStream> T getSourceOutputStream();
    /**
     * {@inheritDoc}
     */
    <T extends OutputStream> void setSourceOutputStream(T os);


}
