package fr.cnam.foad.nfa035.badges.wallet.model;

import java.util.Objects;
/**
 * Classe permettant d'héberger les métadonneées d'un badge
 */
public class DigitalBadgeMetadata {

    int badgeId;
    long imageSize;
    long walletPosition;

        /**
         * Constructor : creer instance meta Data à partir de ses données éleémentaires
         * @param badgeId  : badge identity
         * @param imageSize : taille du badge
         * @param walletPosition  : position du badge dans le wallet
         */
        public void DigitalBadgeMetaData(int badgeId,
                                     long imageSize,
                                     long walletPosition) {
            this.walletPosition = walletPosition;
            this.badgeId = badgeId;
            this.imageSize = imageSize;

        }
        /**
         * Constructeur :creer instance meta Data en initialisant les méta données
         *
         */
        public DigitalBadgeMetadata() {
            this.walletPosition = 0;
            this.badgeId = 0;
            this.imageSize = 0;
        }

        /**
         *
         * @return badgeId
         */
        public int getBadgeId() {
            return badgeId;
        }

        /**
         *
         * @return taille du badge (image)
         */
        public long getImageSize() {
            return imageSize;
        }

        /**
         *
         * @return position du badge dans le wallet
         */
        public long getWalletPosition() {
            return walletPosition;
        }

        /**
         *  renseigner badgeId
         * @param badgeId
         */

        public void setBadgeId(int badgeId) {
            this.badgeId = badgeId;
        }
    /**
     *  renseigner imageSize
     * @param imageSize : taille du badge
     */
        public void setImageSize(long imageSize) {
            this.imageSize = imageSize;
        }
    /**
     *  renseigner badgeId
     * @param walletPosition : position du badge dans le wallet
     */
        public void setWalletPosition(long walletPosition) {
            this.walletPosition = walletPosition;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            DigitalBadgeMetadata that = (DigitalBadgeMetadata) o;
            return getBadgeId() == that.getBadgeId() && getImageSize() == that.getImageSize() && getWalletPosition() == that.getWalletPosition();
        }

        @Override
        public int hashCode() {
            return Objects.hash(getBadgeId(), getImageSize(), getWalletPosition());
        }

        @Override
        public String toString() {
            return "DigitalBadgeMetaData{" +
                    "badgeId=" + badgeId +
                    ", imageSize=" + imageSize +
                    ", walletPosition=" + walletPosition +
                    '}';
        }
}
