package fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer;

import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.WalletFrameMedia;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadgeMetadata;

import java.io.IOException;
import java.util.List;

/**
 *  définit une interface pour deserialiser à partir des meta données
 */
public interface MetadataDeserializer {
    /**
     *
     * @param media
     * @return liste indexee sur meta données
     * @throws IOException
     */
    List<DigitalBadgeMetadata> deserialize(WalletFrameMedia media) throws IOException;
}
