package fr.cnam.foad.nfa035.badges.wallet.dao.impl;

import fr.cnam.foad.nfa035.badges.wallet.dao.DirectAccessBadgeWalletDAO;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.impl.WalletFrame;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.ImageStreamingSerializer;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.impl.ImageDeserializerBase64DatabaseImpl;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.impl.ImageSerializerBase64DatabaseImpl;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadgeMetadata;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import java.util.Collections;
import java.util.List;

public class DirectAccessBadgeWalletDAOImpl
    implements DirectAccessBadgeWalletDAO {
        private final File walletDatabase;

   public DirectAccessBadgeWalletDAOImpl(String dbPath) throws IOException {
            this.walletDatabase = new File(dbPath);
        }

        //@Override
        public void addBadge(File image) throws Exception {
            try (WalletFrame media = new WalletFrame(new RandomAccessFile(walletDatabase, "rw"))) {
                ImageStreamingSerializer serializer = new ImageSerializerBase64DatabaseImpl();
                serializer.serialize(image, media);
            }
        }


        public void getBadge(OutputStream imageStream) throws IOException {
            try (WalletFrame media = new WalletFrame(new RandomAccessFile(walletDatabase, "r"))) {
                new ImageDeserializerBase64DatabaseImpl(imageStream).deserialize(media);
            }
        }

        @Override
        public List<DigitalBadgeMetadata> getWalletMetadata() throws IOException {
            List<DigitalBadgeMetadata> metas = this.getWalletMetadata();
            try (WalletFrame media = new WalletFrame(new RandomAccessFile(walletDatabase, "r"))) {

            }
            // List<DigitalBadgeMetadata> metas = this.getWalletMetadata();
            return Collections.unmodifiableList(metas);
        }

        @Override
        public void getBadgeFromMetadata(OutputStream imageStream, DigitalBadgeMetadata meta) throws IOException {
            List<DigitalBadgeMetadata> metas = this.getWalletMetadata();
            try (WalletFrame media = new WalletFrame(new RandomAccessFile(walletDatabase, "rws"))) {
                // A COMPLéTER
            }
        }

    }
